
/* ================ Model ===================*/

var model = {
  admin: false,
  currentCat: null,
  cats: [
    {
      name: 'Cat1',
      clickCount: 0,
      img: 'img/cat_picture1.jpg'
    },
    {
      name: 'Cat2',
      clickCount: 0,
      img: 'img/cat_picture2.jpeg'
    },
    {
      name: 'Cat3',
      clickCount: 0,
      img: 'img/cat_picture3.jpeg'
    },
    {
      name: 'Cat4',
      clickCount: 0,
      img: 'img/cat_picture4.jpeg'
    },
    {
      name: 'Cat5',
      clickCount: 0,
      img: 'img/cat_picture5.jpeg'
    }
  ]
};


/* =================== Octopus =====================*/

var octopus = {
  init: function(){
    model.currentCat = model.cats[0];
    catListView.init();
    catView.init();
  },
  getCurrentCat: function(){
    return model.currentCat;
  },
  getCats: function(){
    return model.cats;
  },
  setCurrentCat: function(cat){
    model.currentCat = cat
  },
  incrementCounter: function(){
    model.currentCat.clickCount++;
    catView.render();
  },
  saveCat: function(){
    var currentCat = octopus.getCurrentCat();
    currentCat.name = $('#catName').val();
    currentCat.img = $('#imgURL').val();
  },
  isAdmin: function(){
    return model.admin;
  },
  showAdmin: function(){
    model.admin = true;
  },
  cancelAdmin: function(){
    model.admin = false;
  }
};


/* ============== View ============================*/
var catEditView = {
  init: function(){
    this.editCatNameElement = $('#catName');
    this.editImgElement = $('#imgURL');
    this.editClickCount = $('#clicks');
    this.btnSave = $('#btnSave');
    this.btnCancel = $('#btnCancel');

    this.btnSave.click(function(){
      octopus.saveCat()
      catListView.render();
      catView.render();
    });

    this.btnCancel.click(function(){
      octopus.cancelAdmin();
      $('#adminPanel').hide();
    })

    this.render();
  },
  render: function(){
    var currentCat = octopus.getCurrentCat();
    this.editCatNameElement.val(currentCat.name);
    this.editImgElement.val(currentCat.img);
    this.editClickCount.val(currentCat.clickCount);
  }
};

var catView = {
  init: function(){
    this.catElement = $('#cat');
    this.catNameElement = $('#cat-name');
    this.catImageElement = $('#cat-img');
    this.countElement = $('#cat-count');

    this.btnAdmin = $('#btnAdmin');

    this.catImageElement.click(function(){
      octopus.incrementCounter();
    });

    this.btnAdmin.click(function(){
      octopus.showAdmin();
      catView.render();
    });
    this.render();

  },
  render: function(){
    var currentCat = octopus.getCurrentCat();
    this.countElement.text( currentCat.clickCount);
    this.catNameElement.text(currentCat.name);
    this.catImageElement.attr('src', currentCat.img);

    if(octopus.isAdmin()){
      $('#adminPanel').show();
      catEditView.init();
    }else{
      $('#adminPanel').hide();
    }

  }
};

var catListView = {
  init: function(){
    this.catListElement = $('#cat-list');
    this.render();
  },
  render: function(){
    var cat, elem, i;
    var cats = octopus.getCats();

    $('#cat-list').html("");

    for (i=0 ; i<cats.length; i++){
      cat = cats[i];

      // ESSA PARTE EU COPIEI.. TEM QUE CORRIGIR UTILIZANDO JQUERY
      //// TODO: CORRIGIR

      // make a new cat list item and set its text
      elem = document.createElement('li');
      elem.textContent = cat.name;

      // on click, setCurrentCat and render the catView
      // (this uses our closure-in-a-loop trick to connect the value
      //  of the cat variable to the click event function)
      elem.addEventListener('click', (function(catCopy) {
          return function() {
              octopus.setCurrentCat(catCopy);
              catView.render();
          };
      })(cat));

      // finally, add the element to the list
      this.catListElement.append(elem);
    }
  }
};

octopus.init();
